<?php

spl_autoload_register(function ($classe) {
    $classe = str_replace('GabrielSantos\\App\\', 'api/src/', $classe);
    $classe = str_replace('\\', DIRECTORY_SEPARATOR, $classe);
    $arquivoClasse = $classe . '.php';

    if (file_exists($arquivoClasse)) {
        require_once $arquivoClasse;
    }

});