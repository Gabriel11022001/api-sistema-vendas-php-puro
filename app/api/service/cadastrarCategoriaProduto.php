<?php

use GabrielSantos\App\Http\Request;
use GabrielSantos\App\Http\Response;
use GabrielSantos\App\Repositorio\CategoriaProdutoRepositorio;
use GabrielSantos\App\Utils\Db;
use GabrielSantos\App\Utils\ValidaDados;

try {
    $conexaoBancoDados = Db::getConexao();
    $categoria = Request::getDadosRequisicao();
    $validacaoDados = ValidaDados::validarDadosCategoriaProduto($categoria);

    if (count($validacaoDados) > 0) {
        Response::response($validacaoDados, 400);
    } else {
        $categoriaRepositorio = new CategoriaProdutoRepositorio($conexaoBancoDados);
        
        if ($categoriaRepositorio->cadastrarCategoriaDeProduto($categoria)) {
            Response::response(
                [
                    'id' => $conexaoBancoDados->lastInsertId(),
                    'descricao' => $categoria->descricao,
                    'ativo' => $categoria->ativo
                ],
                201
            );
        } else {
            Response::response(
                'Ocorreu um erro ao tentar-se cadastrar a categoria no'
                . ' banco de dados!',
                500
            );
        }

    }

} catch (Exception $e) {
    Response::response($e->getMessage(), 500);
}