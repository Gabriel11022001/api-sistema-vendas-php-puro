<?php

use GabrielSantos\App\Http\Response;
use GabrielSantos\App\Repositorio\CategoriaProdutoRepositorio;
use GabrielSantos\App\Utils\Db;

try {
    $conexaoBancoDados = Db::getConexao();
    $categoriaRepositorio = new CategoriaProdutoRepositorio($conexaoBancoDados);
    $categorias = $categoriaRepositorio->buscarCategoriasDeProduto();
    Response::response($categorias, 200);
} catch (Exception $e) {
    Response::response($e->getMessage(), 500);
}