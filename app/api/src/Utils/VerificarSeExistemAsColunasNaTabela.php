<?php

namespace GabrielSantos\App\Utils;

use GabrielSantos\App\Exceptions\ColunaTabelaInexistenteException;

class VerificarSeExistemAsColunasNaTabela
{

    public static function verificar($tabela, $colunasTabela, $colunasConsulta) {

        foreach ($colunasConsulta as $colunaConsulta) {

            if (in_array($colunaConsulta, $colunasTabela) === false) {
                throw new ColunaTabelaInexistenteException(
                    'A coluna ' . $colunaConsulta . ' não existe na tabela ' . $tabela
                );
            }

        }

    }
}