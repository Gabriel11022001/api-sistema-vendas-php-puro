<?php

namespace GabrielSantos\App\Utils;

use GabrielSantos\App\Exceptions\NaoExisteArquivoRequisicaoException;

class VerificaSeExisteArquivo
{

    public static function verificarSeExisteArquivoDeRequisicao($endpoint) {
        $arquivo = '';

        if (strpos($endpoint, '?')) {
            $arquivo = explode('?', $endpoint)[0];
        } else {
            $arquivo = $endpoint;
        }

        $arquivoFinal = 'api/service/' . $arquivo;
        if (file_exists($arquivoFinal) === false) {
            throw new NaoExisteArquivoRequisicaoException('Endpoint inválido!');
        }

        return $arquivoFinal;
    }
}