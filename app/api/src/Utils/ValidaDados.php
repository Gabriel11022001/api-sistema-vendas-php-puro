<?php

namespace GabrielSantos\App\Utils;

class ValidaDados
{

    public static function validarDadosCategoriaProduto($categoria) {
        $erros = [];

        if (property_exists($categoria, 'id')) {

            if (empty($categoria->id)) {
                $erros[] = 'O id da categoria é um dado obrigatório!';
            } elseif ($categoria->id <= 0) {
                $erros[] = 'O id da categoria deve ser maior que 0!';
            }

        }

        if (empty($categoria->descricao)) {
            $erros[] = 'A descrição da categoria é um dado obrigatório!';
        }

        if (empty($categoria->ativo)) {
            $erros[] = 'O dado "ativo" é obrigatório!';
        } elseif (is_bool($categoria->ativo) === false) {
            $erros[] = 'O dado "ativo" deve ser igual a "true" ou "false"!';
        }

        return $erros;
    }
}