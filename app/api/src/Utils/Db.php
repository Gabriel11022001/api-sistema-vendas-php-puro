<?php

namespace GabrielSantos\App\Utils;

use PDO;

class Db
{

    public static function getConexao() {
        $usuarioBanco = 'postgres';
        $senhaUsuarioBanco = 'root';
        $servidor = 'database';
        $bancoDados = 'db-api-sistema-vendas-php-puro';
        $pdo = new PDO('pgsql:host=' . $servidor. ';dbname=' . $bancoDados, $usuarioBanco, $senhaUsuarioBanco);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return $pdo;
    }
}