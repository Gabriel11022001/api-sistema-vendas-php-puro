<?php

namespace GabrielSantos\App\Http;

use GabrielSantos\App\Exceptions\NaoExisteArquivoRequisicaoException;
use GabrielSantos\App\Utils\VerificaSeExisteArquivo;

class Api
{

    public static function processarRequisicao($endpoint, $metodoHttp) {

        if ($metodoHttp === 'GET') {
            self::get($endpoint);

            return;
        }

        if ($metodoHttp === 'POST') {
            self::post($endpoint);

            return;
        }

        if ($metodoHttp === 'PUT') {
            self::put($endpoint);

            return;
        }

        if ($metodoHttp === 'DELETE') {
            self::delete($endpoint);

            return;
        }

    }

    /**
     * Requisições http utilizando o método
     * http get
     */
    private static function get($endpoint) {

        try {
            $arquivo = VerificaSeExisteArquivo::verificarSeExisteArquivoDeRequisicao($endpoint);
            require_once $arquivo;
        } catch (NaoExisteArquivoRequisicaoException $e) {
            Response::response($e->getMessage(), 404);
        }

    }

    /**
     * Requisições http utilizando o método
     * http post
     */
    private static function post($endpoint) {
        
        try {
            $arquivo = VerificaSeExisteArquivo::verificarSeExisteArquivoDeRequisicao($endpoint);
            require_once $arquivo;
        } catch (NaoExisteArquivoRequisicaoException $e) {
            Response::response($e->getMessage(), 404);
        }

    }

    /**
     * Requisições http utilizando o método
     * http put
     */
    private static function put($endpoint) {

        try {
            $arquivo = VerificaSeExisteArquivo::verificarSeExisteArquivoDeRequisicao($endpoint);
            require_once $arquivo;
        } catch (NaoExisteArquivoRequisicaoException $e) {
            Response::response($e->getMessage(), 404);
        }

    }

    /**
     * Requisições http utilizando o método
     * http delete
     */
    private static function delete($endpoint) {

        try {
            $arquivo = VerificaSeExisteArquivo::verificarSeExisteArquivoDeRequisicao($endpoint);
            require_once $arquivo;
        } catch (NaoExisteArquivoRequisicaoException $e) {
            Response::response($e->getMessage(), 404);
        }
        
    }
}