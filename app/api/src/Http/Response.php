<?php

namespace GabrielSantos\App\Http;

class Response
{

    public static function response($conteudo, $codigoHttp) {
        http_response_code($codigoHttp);
        echo json_encode($conteudo);
    }
}