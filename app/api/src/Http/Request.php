<?php

namespace GabrielSantos\App\Http;

class Request
{

    public static function getDadosRequisicao() {
        $dados = file_get_contents('php://input');
        $dadosObjeto = json_decode($dados);

        return $dadosObjeto;
    }
}