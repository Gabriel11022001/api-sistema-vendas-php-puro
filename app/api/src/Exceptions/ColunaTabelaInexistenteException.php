<?php

namespace GabrielSantos\App\Exceptions;

use Exception;

class ColunaTabelaInexistenteException extends Exception
{
    
    public function __construct($mensagem) {
        parent::__construct($mensagem);
    }
}