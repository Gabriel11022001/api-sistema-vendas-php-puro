<?php

namespace GabrielSantos\App\Repositorio;

use GabrielSantos\App\Utils\VerificarSeExistemAsColunasNaTabela;
use PDO;

class CategoriaProdutoRepositorio
{
    private $conexaoBancoDados;

    public function __construct($conexaoBancoDados) {
        $this->conexaoBancoDados = $conexaoBancoDados;
    }

    public function cadastrarCategoriaDeProduto($categoria) {
        $sql = 'INSERT INTO tb_categoria_produto(descricao, ativo)'
        . ' VALUES(:descricao, :ativo);';
        $stmt = $this->conexaoBancoDados->prepare($sql);
        $stmt->bindValue(':descricao', $categoria->descricao);
        $stmt->bindValue(':ativo', $categoria->ativo, PDO::PARAM_BOOL);

        return $stmt->execute();
    }

    public function buscarCategoriasDeProduto($colunas = []) {
        $sql = 'SELECT ';

        if (count($colunas) === 0) {
            $sql .= ' * ';
        } else {
            $colunasExistentesNaTabela = ['id', 'descricao', 'ativo'];
            VerificarSeExistemAsColunasNaTabela::verificar(
                'tb_categoria_produto',
                $colunasExistentesNaTabela,
                $colunas
            );
            $colunasProntas = '';

            foreach ($colunas as $indice => $coluna) {
                if ($indice === count($colunas) - 1) {
                    $colunasProntas .= $coluna;
                } else {
                    $colunasProntas .= $coluna . ',';
                }
            }

            $sql .= $colunasProntas;
        }

        $sql .= ' FROM tb_categoria_produto;';
        $stmt = $this->conexaoBancoDados->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}