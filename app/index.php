<?php

require_once 'autoload.php';
require_once 'api/config_header.php';

use GabrielSantos\App\Http\Api;

$metodoHttp = $_SERVER['REQUEST_METHOD'];
$endpoint = $_SERVER['REQUEST_URI'];
$endpoint = str_replace('/index.php/', '', $endpoint);
Api::processarRequisicao($endpoint, $metodoHttp);