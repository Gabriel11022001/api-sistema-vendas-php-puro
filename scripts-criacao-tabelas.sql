create table tb_categoria_produto(
	id serial primary key not null,
	descricao text not null,
	ativo boolean not null default true
);